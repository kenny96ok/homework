package homework.collection;

import homework.points.Point;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyListCollectionTest {
    MyListCollection<Point> points = new MyListCollection<>(
            point -> {
                System.out.println("Add " + point.toString());
                return point;
            },
            point -> {
                System.out.println("Remove " + point.toString());
                return point;
            });
    List<Point> list = new ArrayList<>();

    MyListCollectionTest() {
        list.add(new Point(1, 1));
        list.add(new Point(2, 2));
    }

    @Test
    void shouldAddPoints() {
        assertTrue(points.add(new Point(1, 1)));
        assertTrue(points.add(new Point(2, 2)));
        assertEquals(2, points.size());
        assertFalse(points.isEmpty());
    }

    @Test
    void shouldRemovePoints() {
        points.add(new Point(1, 1));
        points.add(new Point(2, 2));
        assertTrue(points.remove(new Point(1, 1)));
        assertTrue(points.remove(new Point(2, 2)));
        assertTrue(points.isEmpty());
    }

    @Test
    void shouldAddAllPoints() {
        assertTrue(points.addAll(list));
        assertEquals(2, points.size());
        assertFalse(points.isEmpty());
        assertArrayEquals(list.toArray(), points.toArray());
    }

    @Test
    void shouldRemoveAllPoints() {
        points.addAll(list);
        assertTrue(points.removeAll(list));
        assertTrue(points.isEmpty());
    }

    @Test
    void shouldContainsPoints() {
        points.addAll(list);
        assertTrue(points.contains(new Point(1, 1)));
        assertTrue(points.contains(new Point(2, 2)));
        assertFalse(points.contains(new Point(3, 3)));
    }

    @Test
    void shouldContainsAllPoints() {
        points.addAll(list);
        assertTrue(points.containsAll(list));
    }

    @Test
    void equalsToArray() {
        points.addAll(list);
        assertArrayEquals(list.toArray(), points.toArray());
    }

    @Test
    void equalsArrays() {
        points.addAll(list);
        assertEquals(points, list);
    }
}