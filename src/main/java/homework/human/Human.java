package homework.human;

public class Human {
    private String FirstName;
    private String SurName;
    private String Patronymic;
    public Human(String FirstName, String SurName)
    {
        this.FirstName=FirstName;
        this.SurName=SurName;
    }
    public Human(String FirstName, String SurName, String Patronymic)
    {
        this.FirstName=FirstName;
        this.SurName=SurName;
        this.Patronymic=Patronymic;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public String getPatronymic() {
        return Patronymic;
    }

    public String getFullName()
    {
        return SurName+" "+FirstName+(Patronymic!=null?" "+Patronymic:"");
    }

    public String getShortName()
    {
        return SurName+" "+FirstName.substring(0,1)+(Patronymic!=null?". "+Patronymic.substring(0,1):"")+".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Human human = (Human) o;

        if (!FirstName.equals(human.FirstName))
            return false;
        if (!SurName.equals(human.SurName))
            return false;
        //Тернарный оператор больше нравится(хуже читается конечно),
        // чем: return Objects.equals(Patronymic, human.Patronymic);
        return Patronymic != null ? Patronymic.equals(human.Patronymic)
                : human.Patronymic == null;
    }

    @Override
    public int hashCode() {
        int result = FirstName.hashCode();
        result = 31 * result + SurName.hashCode();
        result = 31 * result + (Patronymic != null ? Patronymic.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "FirstName='" + FirstName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Patronymic='" + Patronymic + '\'' +
                '}';
    }
}
