package homework.human;

public class Main {
    public static void main(String[] args) {
        Human man=new Human("Олег", "Присажнюк", "Васильевич");
        Human woman=new Human("Валентина", "Стрижаченко");
        System.out.println(man.getFullName());
        System.out.println(woman.getFullName());
        System.out.println(man.getShortName());
        System.out.println(woman.getShortName());

        System.out.println(man.toString());
        System.out.println(woman.toString());
    }
}
