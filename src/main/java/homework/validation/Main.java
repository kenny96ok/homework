package homework.validation;

public class Main {
    public static boolean validation(String login, String password, String confirmPassword) {
        try {
            if (!login.matches("\\w{5,20}"))
                throw new WrongLoginException("Invalid login");
            if (!password.equals(confirmPassword))
                throw new WrongPasswordException("Password and confirmation password do not match");
            if (!password.matches("\\w{8,20}") || !password.matches("\\w*\\d+\\w*")) {
                throw new WrongPasswordException("Invalid password");
            }
        } catch (WrongLoginException e) {
            e.printStackTrace();
            return false;
        } catch (WrongPasswordException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(validation("_login123", "password1", "password1"));
        System.out.println(validation("login@gmail.com", "password1", "password1"));
        System.out.println(validation("_login123", "password", "password"));
    }
}