package homework.validation;

public class WrongLoginException extends RuntimeException {
    public WrongLoginException() {
        super("Invalid login");
    }

    public WrongLoginException(String message) {
        super(message);
    }
}
