package homework.menupoints;

public interface MenuItem {
    void run();
    String getName();
}
