package homework.menupoints;

import homework.points.PointList;

import java.io.OutputStream;
import java.io.PrintStream;

public class ShowAllPointsMenuItem implements MenuItem{
    PointList points;
    PrintStream printer;
    public ShowAllPointsMenuItem(PointList points, OutputStream outputStream) {
        this.points=points;
        this.printer=new PrintStream(outputStream);
    }
    @Override
    public void run() {
        printer.println("Список всех точек:");
        for (int i=0;i<points.size();i++) {
            System.out.printf("%d. %s\n",i+1, points.getPointByIndex(i).print());
        }
    }

    @Override
    public String getName() {
        return "Посмотреть список точек";
    }
}
