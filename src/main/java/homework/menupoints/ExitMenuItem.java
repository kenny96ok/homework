package homework.menupoints;

import static java.lang.System.exit;

public class ExitMenuItem implements MenuItem{
    @Override
    public void run() {
        exit(0);
    }

    @Override
    public String getName() {
        return "Выход";
    }
}
