package homework.menupoints;

import homework.points.Circle;
import homework.points.PointList;

import java.io.OutputStream;
import java.io.PrintStream;

public class PointsOutCircleMenuItem implements MenuItem{
    PointList points;
    Circle circle;
    PrintStream printer;
    public PointsOutCircleMenuItem(PointList points, Circle circle, OutputStream outputStream) {
        this.points = points;
        this.circle = circle;
        this.printer = new PrintStream(outputStream);
    }

    @Override
    public void run() {
        printer.println("Точки находящиеся вне окружности:");
        for(int i=0;i< points.size();i++)
        {
            if(!circle.pointInCircle(points.getPointByIndex(i)))
                printer.println(points.getPointByIndex(i).print());
        }
    }

    @Override
    public String getName() {
        return "Посмотреть список точек вне окружности";
    }
}
