package homework.menupoints;

import homework.points.Circle;
import homework.points.Point;

import java.io.InputStream;
import java.util.Scanner;

public class ChangeCircleMenuItem implements MenuItem{
    Circle circle;
    Scanner scanner;
    public ChangeCircleMenuItem(Circle circle, InputStream inputStream) {
        this.circle=circle;
        scanner=new Scanner(inputStream);
    }
    @Override
    public void run() {
        int x, y;
        System.out.print("Введите координаты центра окружности и радиус:\nx:");
        x=scanner.nextInt();
        System.out.print("y:");
        y=scanner.nextInt();
        System.out.print("R:");
        circle.setRadius(scanner.nextInt());
        circle.setCenter(new Point(x,y));
    }

    @Override
    public String getName() {
        return "Изменить окружность";
    }
}
