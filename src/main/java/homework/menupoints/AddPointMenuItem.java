package homework.menupoints;

import homework.points.PointList;

import java.io.InputStream;
import java.util.Scanner;

public class AddPointMenuItem implements MenuItem{
    PointList points;
    Scanner scanner;
    public AddPointMenuItem(PointList points, InputStream inputStream) {
        this.points=points;
        this.scanner=new Scanner(inputStream);
    }
    @Override
    public void run() {
        int x, y;
        do {
            System.out.print("Введите координаты точки:\nx:");
            x=scanner.nextInt();
            System.out.print("y:");
            y=scanner.nextInt();
            points.addLastPoint(x,y);
            System.out.print("Желаете добавить еще (1-да 2-нет)\nВаш выбор:");
        }while (scanner.nextInt()==1);
    }

    @Override
    public String getName() {
        return "Добавить точку";
    }
}
