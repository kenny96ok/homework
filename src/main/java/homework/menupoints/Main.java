package homework.menupoints;

import homework.points.Circle;
import homework.points.PointList;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PointList points = new PointList();
        Scanner scanner=new Scanner(System.in);
        Circle circle = new Circle(0,0,0);
        MenuItem[] items = { new AddPointMenuItem(points, System.in),
                new ShowAllPointsMenuItem(points,System.out),
                new PointsInCircleMenuItem(points, circle, System.out),
                new PointsOutCircleMenuItem(points, circle, System.out),
                new ChangeCircleMenuItem(circle,System.in),
                new ExitMenuItem()};
        while (true) {
            System.out.println("*".repeat(50));
            for (int i = 0; i < items.length; i++) {
                System.out.printf("%d %s\n", i + 1, items[i].getName());
            }
            System.out.print("Ваш выбор: ");
            int answer = scanner.nextInt()-1;
            System.out.println("*".repeat(50));
            if(answer>=0 && answer<items.length)
                items[answer].run();
            else
                System.out.println("Неправильный ввод!");
        }
    }
}
