package homework.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class MyListCollection<T> implements Collection<T> {
    private OperationInAddition<T> operationInAddition;
    private OperationInRemove<T> operationInRemove;
    private final Collection<T> list = new LinkedList<>();

    public MyListCollection(OperationInAddition<T> operationInAddition, OperationInRemove<T> operationInRemove) {
        this.operationInAddition = operationInAddition;
        this.operationInRemove = operationInRemove;
    }

    @Override
    public boolean add(T t) {
        operationInAddition.action(t);
        return list.add(t);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(elem -> operationInAddition.action((elem)));
        return list.addAll(c);
    }

    @Override
    public boolean remove(Object o) {
        operationInRemove.action(((T) o));
        return list.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        c.forEach(elem -> operationInRemove.action(((T) elem)));
        return list.removeAll(c);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean equals(Object o) {
        return list.equals(o);
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }
}
