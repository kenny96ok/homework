package homework.collection;

@FunctionalInterface
public interface OperationInAddition<T> {
    T action(T t);
}
