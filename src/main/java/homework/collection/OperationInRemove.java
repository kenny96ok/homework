package homework.collection;

@FunctionalInterface
public interface OperationInRemove<T> {
    T action(T t);
}
