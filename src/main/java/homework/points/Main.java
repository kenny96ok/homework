package homework.points;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PointList points=new PointList();
        Scanner scanner = new Scanner(System.in);
        int x, y, R;
        do {
            System.out.print("Введите координаты точки:\nx:");
            x=scanner.nextInt();
            System.out.print("y:");
            y=scanner.nextInt();
            points.addLastPoint(x,y);
            System.out.print("Желаете добавить еще (1-да 2-нет)\nВаш выбор:");
        }while (scanner.nextInt()==1);
        System.out.print("Введите координаты центра окружности и радиус:\nx:");
        x=scanner.nextInt();
        System.out.print("y:");
        y=scanner.nextInt();
        System.out.print("R:");
        R=scanner.nextInt();
        Circle circle=new Circle(x,y,R);
        System.out.println("Точки находящиеся в окружности:");
        for(int i=0;i< points.size();i++)
        {
            if(circle.pointInCircle(points.getPointByIndex(i)))
                System.out.printf("Точка(%d;%d)\n",points.getPointByIndex(i).getX(),points.getPointByIndex(i).getY());
        }
    }
}
