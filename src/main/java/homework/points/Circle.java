package homework.points;

public class Circle {
    private Point center;
    private int radius;
    public Circle(int x, int y, int radius)
    {
        center = new Point(x,y);
        this.radius = radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setCenter(Point center) {
        this.center=center;
    }

    public int getRadius() {
        return radius;
    }

    public Point getCenter() {
        return center;
    }

    public boolean pointInCircle(Point p) {
        return radius >= center.distanceTo(p.getX(),p.getY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (radius != circle.radius) return false;
        return center.equals(circle.center);
    }

    @Override
    public int hashCode() {
        int result = center.hashCode();
        result = 31 * result + radius;
        return result;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "center=" + center.toString() +
                ", radius=" + radius +
                '}';
    }
}
