package homework.points;


public class PointList {
    private Point[] array;
    private int count;

    public PointList() {
        array=null;
        count=0;
    }

    public void addLastPoint(int x, int y) {
        if(count==0) {
            array = new Point[++count];
            array[0]=new Point(x, y);
            return;
        }
        Point[] temp=array;
        array = new Point[count+1];
        for(int i=0; i<count; i++) {
            array[i]=temp[i];
        }
        array[count++]=new Point(x, y);
    }

    public void addFirstPoint(int x, int y) {
        if(count==0) {
            array = new Point[++count];
            array[0]=new Point(x, y);
            return;
        }
        Point[] temp=array;
        array = new Point[++count];
        array[0]=new Point(x, y);
        for(int i=1; i<count; i++) {
            array[i]=temp[i-1];
        }
    }

    public int size() {
        return count;
    }

    public Point getPointByIndex(int index)
    {
        return (index < 0 || index >= count)?null:array[index];
    }
}
