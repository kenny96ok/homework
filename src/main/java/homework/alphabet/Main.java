package homework.alphabet;

public class Main {
    public static void main(String[] args) {
        for (Alphabet value : Alphabet.values()) {
            System.out.printf("Позиция буквы %s: %d\n", value.name(), value.getLetterPosition());
        }
    }
}
